# The Solid Principles

# S - Single Responsibility - SRP

You're Objects should a have a single responsibility.
Or in other words an object needs only to fill one role in your application.

So say you have an object that is used to create a car object.
Well that car object fullfills the role of being your car.
It may have many more objects internally that help build that role,
but when a consumer wants a car they don't need to know about all the objects
they only need to car about a car. And the car fills that role.

Now, should a car no how to drive itself?
I'm not talking about the future here, but I am asking if a car
currently knows how to drive itself? The answer is no. Something else
knows how to drive the car, the car provides all the api a user of a car needs
to use it properly. And that User fills the role of the driver.

Now that's a good example of how to tell when you're objects are doing to much.
You can think of any other objects as cars, and whether or not you've given them
the behavior to "drive themselves".

Now remember, cars are abstractions. Which means you don't need to think that just
because you're car is made of a bunch of parts, that's doing to much.
The abstraction the car makes is still a single role, with a single responsibility.

# O - Open / Closed - OCP
