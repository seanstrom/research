# Testing Terminology

## Lingo

**Unit Testing**
So we you may know what a unit test is, if not I will probably have another composed
write up about that as well. But the gist is, a unit test is an isolated test.
It exists to test a specific item or unit. That may well be an overly simple description of it
but it serves as a good contrast for the next definition.


**Functional Testing**
This form of testing is similar to how we sometimes write our unit tests.
You can imagine that these kinds of tests are the ones that tests command type tests.
Like we called function X with number 1.
In that example were testing the funcionality of our module with function X.


**Integration Testing**
By some peoples definitions an integration test is not a full stack test.
Unless someone says Full Stack Integration test.
But integration tests are used to test the integration between any two or more layers.
Like if your function writes to disk, you need to test that it was written to disk
and yadda yadda.


**Acceptance Testing**
Okay, an acceptance test will test the whole stack all the way through.
Browser to DB and back for instance. These tests are typically the ones that
are typically referred to as user stories, and represent fully complete features of
software. These are at the outer rim of the feedback loop.


**Feature Testing**
Synonymous with Acceptance Testing
