# Testing Tools in Node

## Intro
So in the world of software development there exists an idea that
when you write your software you should also be writing tests.
Now some agree to write them after their code and some agree to write them
before they write there code. Oh yes very interesting I know, but none the less
if you are in the business of writing tests, more specifically in node, come read
about the tools you could use to help write tests.

## Test Runners

* mocha

## Browser Testing
* phantom js
* zombie js
* slimer js
