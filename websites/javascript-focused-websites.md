# Websites focused on Javascript and Javascript Related Material

# Javascript libraries catalogue
jster.net

#node
http://www.nodecloud.org/
http://nodewebmodules.com/
http://codecondo.com/7-minimal-node-js-web-frameworks/
http://nodejsreactions.tumblr.com/
https://github.com/Pana/node-books

# js
http://jstherightway.org/
http://jsbooks.revolunet.com/
http://www.webappers.com/

# node and js
http://www.echojs.com/
https://javascriptkicks.com/
